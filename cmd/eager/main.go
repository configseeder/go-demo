package main

import (
	"flag"
	log "github.com/sirupsen/logrus"
	"gitlab.com/configseeder/go-client"
	"gitlab.com/configseeder/go-client/config"
	"gitlab.com/configseeder/go-demo/tablewriter"
	"os"
)

func main() {
	// Setup Logging
	log.SetFormatter(&log.TextFormatter{})
	log.SetOutput(os.Stdout)
	log.SetLevel(log.DebugLevel)

	// Setup Flags for ConfigSeeder access configuration
	client := configseeder.ClientImplementation{}
	client.InitFlags()
	_ = flag.Set(config.ConfigseederConfigfile, "configseeder-eager.yaml") // Normally not required, programmatically override name of configseeder config file
	flag.Parse()

	// initialize go client
	err := client.Init(nil)
	if err != nil {
		log.Fatal(err)
	}

	baseUrl, ok := client.GetWellKnown().GetUrl("baseUrl")
	if ok {
		log.Infof("Base url: %s", baseUrl)
	} else {
		log.Warnf("Unable to get base url")
	}

	// Get data from ConfigSeeder
	configValueDtos, err := client.GetConfigValues()
	if err != nil {
		log.Warn(err)
	}

	tablewriter.PrintConfigValues(configValueDtos)
}
