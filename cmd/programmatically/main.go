package main

import (
	log "github.com/sirupsen/logrus"
	configseeder "gitlab.com/configseeder/go-client"
	"gitlab.com/configseeder/go-client/config"
	"gitlab.com/configseeder/go-client/model"
	"gitlab.com/configseeder/go-client/util"
	"gitlab.com/configseeder/go-demo/tablewriter"
	"os"
)

func main() {
	// Setup Logging
	log.SetFormatter(&log.TextFormatter{})
	log.SetOutput(os.Stdout)
	log.SetLevel(log.DebugLevel)

	// Setup config
	apiKey := "eyJraWQiOiJjb25maWdzZWVkZXItand0LWlkIiwiYWxnIjoiUlM1MTIifQ.eyJ0eXBlIjoiYXBpa2V5IiwidHlwZSI6ImFwaS1rZXkiLCJpc3MiOiJDb25maWdTZWVkZXJNYW5hZ2VtZW50IiwiaWF0IjoxNTg0MzcxMzY5LCJleHAiOjE2MDk0NTkyMDAsInN1YiI6IkdvLUNsaWVudCBEZW1vIFRlc3Rncm91cCIsImFwaS10b2tlbi1pZCI6ImM5ZjUyMjcyLWQ4MmYtNGUxOS05ZTQyLWU0ZjBmYzk0ZTYxNCIsInRlbmFudC1pZCI6IjVhZmY3ZTFiLTM3ZDYtNGJmZC1hODBmLTVmN2UzOTA2M2U1MSIsImFwaS1rZXktdHlwZSI6IkNMSUVOVCIsImFjY2VzcyI6eyJhbGxFbnZpcm9ubWVudHMiOnRydWUsImFsbENvbmZpZ3VyYXRpb25Hcm91cHMiOmZhbHNlLCJjb25maWd1cmF0aW9uLWdyb3VwcyI6W3siaWQiOiIwMWY0ZmNmOC02YzBkLTQyM2ItOGQ0NS0xZGZiMmM3Y2E1YWQiLCJrZXkiOiJnby1kZW1vLXRlc3Rncm91cCJ9XX19.J4p7ABZT__91RoSWACbOkY4_malqZ7VvhKhxfC5FFZEDvDvmjTHvvLuX0jQQCZlah8TSPKYwxi10kdSzXydPQGrJypBn2HUF6GvETA2qAnkcGVsZ_bb4OR4y2YxBzjpJy6es4sQVa_0G2VVJ7AqGtxGFpjXLYXsKteJKtynCAbM"
	dateTime, err := util.FormattetTimeWithZoneToTime(getStrPointer("2020-02-01T01:02:03Z"))
	if err != nil {
		log.Fatalf("error parsing date", err)
	}
	initMode := config.InitializationModeEager
	refreshMode := config.RefreshModeManual
	selectionMode := model.SelectionModeLatest
	clientConfig := &config.ClientConfiguration{
		ApiKey: &apiKey,
		Configurations: []config.GroupConfiguration{
			{
				Key:           "go-demo-testgroup",
				SelectionMode: &selectionMode,
			},
		},
		ConnectionTimeout:  getIntPointer(2000),
		DateTime:           dateTime,
		FullForNonFiltered: getBoolPointer(false),
		EnvironmentKey:     getStrPointer("TEST"),
		InitializationMode: &initMode,
		MaxRetries:         getIntPointer(3),
		ReadTimeout:        getIntPointer(2000),
		RefreshCycle:       getIntPointer(15000),
		RefreshMode:        &refreshMode,
		ServerUrl:          getStrPointer("https://staging-postgres-config-seeder.oneo.cloud"),
		TenantKey:          "default",
	}

	// Init client programmatically
	client := configseeder.ClientImplementation{}
	err = client.InitWith(clientConfig, nil, nil)
	if err != nil {
		log.Fatalf("Error when initializing go-client", err)
	}

	// Get data from ConfigSeeder
	configValueDtos, err := client.GetConfigValues()
	if err != nil {
		log.Warn(err)
	}

	tablewriter.PrintConfigValues(configValueDtos)
}

func getBoolPointer(value bool) *bool {
	return &value
}
func getIntPointer(value int) *int {
	return &value
}
func getStrPointer(value string) *string {
	return &value
}
