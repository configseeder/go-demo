package main

import (
	"flag"
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/configseeder/go-client"
	"gitlab.com/configseeder/go-client/config"
	"gitlab.com/configseeder/go-client/model"
	"gitlab.com/configseeder/go-demo/tablewriter"
	"os"
	"time"
)

func main() {
	// Setup Logging
	log.SetFormatter(&log.TextFormatter{})
	log.SetOutput(os.Stdout)
	log.SetLevel(log.DebugLevel)

	// Setup Flags for ConfigSeeder access configuration
	client := configseeder.ClientImplementation{}
	client.InitFlags()
	_ = flag.Set(config.ConfigseederConfigfile, "configseeder-timer.yaml") // Normally not required, programatically override name of configseeder configfile
	flag.Parse()

	// initialize go client
	err := client.Init(nil)
	if err != nil {
		log.Fatal(err)
	}

	baseUrl, ok := client.GetWellKnown().GetUrl("baseUrl")
	if ok {
		log.Infof("Base url: %s", baseUrl)
	} else {
		log.Warnf("Unable to get base url")
	}

	// Get data from ConfigSeeder
	configValueDtos, err := client.GetConfigValues()
	if err != nil {
		log.Warn(err)
	}

	tablewriter.PrintConfigValues(configValueDtos)

	// Register listener for handling updates
	client.Subscribe(DemoListener)

	// wait / can also be an endless loop
	time.Sleep(180 * time.Second)
}

func DemoListener(changedKeys []model.ConfigValueChange) {
	_, _ = os.Stdout.WriteString("Received Update:\n")
	for _, changedValue := range changedKeys {
		if changedValue.NewValue != nil {
			_, _ = os.Stdout.WriteString(fmt.Sprintf("\tKey %s has now Value '%s'\n", changedValue.NewValue.Key, changedValue.NewValue.Value))
		} else {
			_, _ = os.Stdout.WriteString(fmt.Sprintf("\tKey %s was deleted\n", changedValue.OldValue.Key))
		}
	}
}
