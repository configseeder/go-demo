package main

import (
	"flag"
	log "github.com/sirupsen/logrus"
	"gitlab.com/configseeder/go-client"
	"gitlab.com/configseeder/go-client/config"
	"gitlab.com/configseeder/go-demo/tablewriter"
	"os"
)

func main() {
	// Setup Logging
	log.SetFormatter(&log.TextFormatter{})
	log.SetOutput(os.Stdout)
	log.SetLevel(log.DebugLevel)

	// Setup Flags for ConfigSeeder access configuration
	client := configseeder.ClientImplementation{}
	client.InitFlags()
	_ = flag.Set(config.ConfigseederConfigfile, "configseeder-local-https.yaml") // Normally not required, programatically override name of configseeder configfile
	flag.Parse()

	// initialize go client
	err := client.Init(nil)
	if err != nil {
		log.Fatal(err)
	}

	// Get data from ConfigSeeder
	configValueDtos, err := client.GetConfigValues()
	if err != nil {
		log.Warn(err)
	}

	tablewriter.PrintConfigValues(configValueDtos)
}
