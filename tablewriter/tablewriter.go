package tablewriter

import (
	"fmt"
	"gitlab.com/configseeder/go-client/model"
	"gitlab.com/configseeder/go-client/util"
	"os"
	"text/tabwriter"
)

func PrintConfigValues(configValueDtos []model.ConfigValueDto) {
	if len(configValueDtos) > 0 {
		_, _ = os.Stdout.WriteString(fmt.Sprintf("Received %d ConfigValues from ConfigSeeder:\n\n", len(configValueDtos)))

		writer := tabwriter.NewWriter(os.Stdout, 0, 0, 1, ' ', 0)
		_, _ = fmt.Fprintf(writer, "%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n",
			"key", "type", "value", "secured", "environment", "dateFrom", "dateTo", "versionFrom", "versionTo ", "context")

		for _, configValueDto := range configValueDtos {
			key := configValueDto.Key
			valueType := configValueDto.Type
			value := configValueDto.Value
			secured := configValueDto.Secured
			environment := getValue(configValueDto.Environment)
			dateFrom := getValue(util.TimeToFormattetTimeWithZone(configValueDto.ValidFrom))
			dateTo := getValue(util.TimeToFormattetTimeWithZone(configValueDto.ValidTo))
			versionFrom := getValue(configValueDto.VersionFrom)
			versionTo := getValue(configValueDto.VersionTo)
			context := getValue(configValueDto.Context)

			_, _ = fmt.Fprintf(writer, "%s\t%s\t%s\t%t\t%s\t%s\t%s\t%s\t%s\t%s\n",
				key, valueType, value, secured, environment, dateFrom, dateTo, versionFrom, versionTo, context)
		}

		_ = writer.Flush()
	}
}

func getValue(value *string) string {
	if value != nil {
		return *value
	}
	return ""
}
