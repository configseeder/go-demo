# Go Demo

## Update dependencies

1. Update ConfigSeeder go client
   * Commit Hash: `go get gitlab.com/configseeder/go-client@400d35504e212be065ee4f79de7312bc8c0f5c24`

2. `go mod tidy` 
3. `go mod vendor` 