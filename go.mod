module gitlab.com/configseeder/go-demo

go 1.22

toolchain go1.22.3

require (
	github.com/sirupsen/logrus v1.9.3
	gitlab.com/configseeder/go-client v1.5.0
)

require (
	golang.org/x/sys v0.20.0 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
